package br.com.shinhithi.android_architecture.data;

import android.support.test.runner.AndroidJUnit4;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.os.SystemClock.sleep;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public final class DummyTest {
    @Test
    public void noneOfTheThings() {
        sleep(SECONDS.toMillis(5)); // Long enough to see some data from mock mode.
        assertTrue(true);
    }
}
