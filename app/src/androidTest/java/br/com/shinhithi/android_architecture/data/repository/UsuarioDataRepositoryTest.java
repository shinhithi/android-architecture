package br.com.shinhithi.android_architecture.data.repository;

import br.com.shinhithi.android_architecture.data.ApplicationTestCase;
import br.com.shinhithi.android_architecture.data.entity.UsuarioEntity;
import br.com.shinhithi.android_architecture.domain.Usuario;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import rx.Observable;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

public class UsuarioDataRepositoryTest extends ApplicationTestCase {


    private static final int FAKE_USER_ID = 123;

    private UsuarioDataRepository usuarioDataRepository;

    @Mock
    private UsuarioEntity usuarioEntity;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        usuarioDataRepository = new UsuarioDataRepository(usuarioEntity);
    }

    @Test
    public void testLisaUsuario(){
        List<UsuarioEntity> lista = Lists.newArrayList();
        given(UsuarioEntity.listAll(UsuarioEntity.class)).willReturn(lista);
        assertTrue(true);
    }

}
