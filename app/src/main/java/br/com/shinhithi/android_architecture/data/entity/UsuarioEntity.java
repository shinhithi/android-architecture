package br.com.shinhithi.android_architecture.data.entity;

import com.orm.SugarRecord;

public class UsuarioEntity extends SugarRecord<UsuarioEntity> {

    public String nome;
    public String email;
    public String senha;
    public EnderecoEntity endereco;

    public UsuarioEntity(){}

    public UsuarioEntity(String nome, String email, String senha, EnderecoEntity endereco){
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.endereco = endereco;
    }

    public void atualizar() {
    }
}
