package br.com.shinhithi.android_architecture.data.entity;

import com.orm.SugarRecord;

public class EnderecoEntity extends SugarRecord<EnderecoEntity> {

    String logradouro;
    String numero;
    String bairro;
    String cep;
    String cidade;
    String uf;

    public EnderecoEntity(){}

}
