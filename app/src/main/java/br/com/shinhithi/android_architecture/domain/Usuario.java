package br.com.shinhithi.android_architecture.domain;

public class Usuario {

    public Long id;
    public String nome;
    public String email;
    public String senha;
    public Endereco endereco;

    public Usuario(Long id, String nome, String email, String senha, Endereco endereco){
        this.id = id;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.endereco = endereco;
    }

}
