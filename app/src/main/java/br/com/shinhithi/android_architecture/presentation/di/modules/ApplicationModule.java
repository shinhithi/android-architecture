package br.com.shinhithi.android_architecture.presentation.di.modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import br.com.shinhithi.android_architecture.data.repository.UsuarioDataRepository;
import br.com.shinhithi.android_architecture.domain.repository.UsuarioRepository;
import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    UsuarioRepository provideUsuarioRepository(UsuarioDataRepository usuarioDataRepository){return usuarioDataRepository;}

}
