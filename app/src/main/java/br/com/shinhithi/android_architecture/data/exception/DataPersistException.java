package br.com.shinhithi.android_architecture.data.exception;

public class DataPersistException extends Exception {

    public DataPersistException() {
        super();
    }

    public DataPersistException(final String message) {
        super(message);
    }

    public DataPersistException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DataPersistException(final Throwable cause) {
        super(cause);
    }
}
