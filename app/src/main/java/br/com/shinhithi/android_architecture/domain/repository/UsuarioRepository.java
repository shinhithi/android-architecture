package br.com.shinhithi.android_architecture.domain.repository;

import br.com.shinhithi.android_architecture.domain.Usuario;
import rx.Observable;

import java.util.List;

public interface UsuarioRepository {

    Observable<List<Usuario>> lista();

    Observable<Usuario> busca(final Long id);

    Observable<Usuario> salvar(final Usuario usuario);
}
