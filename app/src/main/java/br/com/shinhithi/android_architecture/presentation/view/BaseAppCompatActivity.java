package br.com.shinhithi.android_architecture.presentation.view;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import br.com.shinhithi.android_architecture.presentation.AndroidApplication;
import br.com.shinhithi.android_architecture.presentation.di.components.ApplicationComponent;
import butterknife.ButterKnife;

public class BaseAppCompatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return ((AndroidApplication) getApplication()).getApplicationComponent();
    }

}
