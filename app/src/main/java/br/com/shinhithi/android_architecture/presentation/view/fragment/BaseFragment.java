package br.com.shinhithi.android_architecture.presentation.view.fragment;

import android.support.v4.app.Fragment;
import br.com.shinhithi.android_architecture.presentation.di.components.ApplicationComponent;

public class BaseFragment extends Fragment {

    public ApplicationComponent getApplicationComponent() {
        return getApplicationComponent();
    }

}
