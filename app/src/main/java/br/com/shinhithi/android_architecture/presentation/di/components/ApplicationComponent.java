package br.com.shinhithi.android_architecture.presentation.di.components;

import android.content.Context;
import br.com.shinhithi.android_architecture.domain.repository.UsuarioRepository;
import br.com.shinhithi.android_architecture.presentation.di.modules.ApplicationModule;
import dagger.Component;

import javax.inject.Singleton;


@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    //void inject(OutputActivity activity);

    UsuarioRepository usuarioRepository();
    //Exposed to sub-graphs.
    Context ApplicationContext();
}
