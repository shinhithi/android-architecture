package br.com.shinhithi.android_architecture.presentation;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import br.com.shinhithi.android_architecture.presentation.di.components.ApplicationComponent;
import br.com.shinhithi.android_architecture.presentation.di.components.DaggerApplicationComponent;
import br.com.shinhithi.android_architecture.presentation.di.modules.ApplicationModule;
import com.orm.SugarApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class AndroidApplication extends SugarApp {

    private ApplicationComponent applicationComponent;
    private static final String namePreferences = "MyPrefsFile";
    private static final String myFirstTime = "my_first_time";
    private static final boolean inBackground = true;
    private static final String sugar_init_file = "sugar_upgrades/init.sql";

    @Override public void onCreate() {
        super.onCreate();
        this.initializeInjector();
        this.initializeDataBase();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

    public void initializeDataBase(){
        if (getSharedPreferences(AndroidApplication.namePreferences, 0).getBoolean(AndroidApplication.myFirstTime, true)) {
            Log.d("Comments", "Primeira execu��o");

            //executeScript(super.getDatabase().getDB());
            if(inBackground) {
                Log.i("Sugar", "script executed in background mode");
                new RunInitScriptTask(this, super.getDatabase().getDB()).execute();
            }else{
                AndroidApplication.executeScript(getApplicationContext(), super.getDatabase().getDB());
            }

            getSharedPreferences(AndroidApplication.namePreferences, 0).edit().putBoolean(AndroidApplication.myFirstTime, false).commit();
        }
    }

    private static void executeScript(Context context, SQLiteDatabase db) {
        try {
            InputStream is = context.getAssets().open(sugar_init_file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                Log.i("Sugar script", line);
                db.execSQL(line.toString());
            }
        } catch (IOException e) {
            Log.e("Sugar", e.getMessage());
        }

        Log.i("Sugar", "script executed");
    }

    private static class RunInitScriptTask extends AsyncTask<Void, Integer, Void> {

        private SQLiteDatabase db;
        private Context context;

        public RunInitScriptTask(Context context, SQLiteDatabase db) {
            this.context = context;
            this.db = db;
        }

        @Override
        protected Void doInBackground(Void... params) {
            AndroidApplication.executeScript(context, db);
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            Log.e("Sugar", "Qtde Scripts=" + String.valueOf(progress[0]));
        }

    }
}
