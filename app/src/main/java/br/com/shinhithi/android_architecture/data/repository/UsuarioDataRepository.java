package br.com.shinhithi.android_architecture.data.repository;

import br.com.shinhithi.android_architecture.data.entity.UsuarioEntity;
import br.com.shinhithi.android_architecture.data.exception.DataNotFoundException;
import br.com.shinhithi.android_architecture.data.exception.DataPersistException;
import br.com.shinhithi.android_architecture.domain.Usuario;
import br.com.shinhithi.android_architecture.domain.repository.UsuarioRepository;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import rx.Observable;
import rx.Subscriber;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class UsuarioDataRepository implements UsuarioRepository {

    private UsuarioEntity entity;

    @Inject
    public UsuarioDataRepository(){
        this.entity = new UsuarioEntity();
    }

    public UsuarioDataRepository(UsuarioEntity usuarioEntity){
        this.entity = usuarioEntity;
    }

    private final Function<UsuarioEntity, Usuario> transformFunc = new Function<UsuarioEntity, Usuario>() {
        @Override
        public Usuario apply(UsuarioEntity usuario) {
            return new Usuario(usuario.getId(), usuario.nome, usuario.nome, usuario.senha, null);
        }
    };

    private final Function<Usuario, UsuarioEntity> transformEntityFunc = new Function<Usuario, UsuarioEntity>() {
        @Override
        public UsuarioEntity apply(Usuario usuario) {
            return new UsuarioEntity(usuario.nome, usuario.nome, usuario.senha, null);
        }
    };

    @Override
    public Observable<List<Usuario>> lista() {
        return Observable.create(new Observable.OnSubscribe<List<Usuario>>() {
            @Override
            public void call(Subscriber<? super List<Usuario>> subscriber) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                List<UsuarioEntity> usuarios = entity.listAll(UsuarioEntity.class);
                if(!usuarios.isEmpty()) {
                    subscriber.onNext(Lists.transform(usuarios, transformFunc));
                    subscriber.onCompleted();
                }else{
                    subscriber.onError(new DataNotFoundException());
                }
            }
        });
    }

    @Override
    public Observable<Usuario> busca(final Long id) {
        return Observable.create(new Observable.OnSubscribe<Usuario>() {
            @Override
            public void call(Subscriber<? super Usuario> subscriber) {
                UsuarioEntity usuario = entity.findById(UsuarioEntity.class, id);
                if(usuario != null) {
                    subscriber.onNext(transformFunc.apply(usuario));
                    subscriber.onCompleted();
                }else{
                    subscriber.onError(new DataNotFoundException());
                }
            }
        });
    }

    @Override
    public Observable<Usuario> salvar(final Usuario usuarioSave) {
        return Observable.create(new Observable.OnSubscribe<Usuario>() {
            @Override
            public void call(Subscriber<? super Usuario> subscriber) {
                UsuarioEntity usuario = entity.findById(UsuarioEntity.class, usuarioSave.id);
                try {
                    if (usuario != null) {
                        usuario.atualizar();
                    } else {
                        usuario = transformEntityFunc.apply(usuarioSave);
                    }
                    usuario.save();
                    subscriber.onNext(transformFunc.apply(usuario));
                    subscriber.onCompleted();
                }catch(Exception e){
                    subscriber.onError(new DataPersistException(e));
                }
            }
        });
    }

}
